# Transposer - A Generalized Process Pipeline

This project contains the main documentation concerning the _Transposer_ project.

You will find further information in the [Wiki](https://git.fairkom.net/emb/displ.eu/transposer/transposer-documentation/-/wikis/home).
